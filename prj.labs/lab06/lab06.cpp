#include <opencv2/opencv.hpp>
#include <vector>
#include <iostream>
#include <limits>
#include <fstream>
#include <nlohmann/json.hpp>
#include <map>

enum PreColourReduction {
	None = 0,
	GrayWorld,
	AutoContrast,
	GrayWorld_AutoContrast,
	AutoContrast_GrayWorld,
	_Size
};

static const std::vector<std::string> key_names = { "", "_gw", "_ac", "_gw-ac", "_ac-gw" };

cv::Mat grayWorld(cv::Mat&& image) 
{
	float B_mean = 0.0f, G_mean = 0.0f, R_mean = 0.0f;

	for (int y = 0; y < image.rows; ++y) {
		for (int x = 0; x < image.cols; ++x) {
			B_mean += image.at<cv::Vec3b>(y, x)[0];
			G_mean += image.at<cv::Vec3b>(y, x)[1];
			R_mean += image.at<cv::Vec3b>(y, x)[2];
		}
	}

	int imageArea = image.size().area();

	R_mean /= imageArea;
	G_mean /= imageArea;
	B_mean /= imageArea;

	float Gray_mean = (R_mean + G_mean + B_mean) / 3;

	float k_r = Gray_mean / R_mean;
	float k_g = Gray_mean / G_mean;
	float k_b = Gray_mean / B_mean;

	for (int y = 0; y < image.rows; ++y) {
		for (int x = 0; x < image.cols; ++x) {
			if (image.at<cv::Vec3b>(y, x)[0] * k_b >= 255) {
				image.at<cv::Vec3b>(y, x)[0] = 255;
			}
			else {
				image.at<cv::Vec3b>(y, x)[0] *= k_b;
			}
			
			if (image.at<cv::Vec3b>(y, x)[1] * k_g >= 255) {
				image.at<cv::Vec3b>(y, x)[1] = 255;
			}
			else {
				image.at<cv::Vec3b>(y, x)[1] *= k_g;
			}
			
			if (image.at<cv::Vec3b>(y, x)[2] * k_r >= 255) {
				image.at<cv::Vec3b>(y, x)[2] = 255;
			}
			else {
				image.at<cv::Vec3b>(y, x)[2] *= k_r;
			}
		}
	}

	return image;
}

cv::Mat autoContrast(cv::Mat&& image) 
{
	const int channelMin = 0, channelMax = 255;
	std::vector<int> channelsLow, channelsHigh;

	// Calculating channels low and high values
	for (int i = 0; i < 3; ++i) {
		channelsLow.push_back(channelMax);
		channelsHigh.push_back(channelMin);

		for (int y = 0; y < image.rows; ++y) {
			for (int x = 0; x < image.cols; ++x) {
				if (image.at<cv::Vec3b>(y, x)[i] < channelsLow.at(i)) {
					channelsLow.at(i) = image.at<cv::Vec3b>(y, x)[i];
				}
				if (image.at<cv::Vec3b>(y, x)[i] > channelsHigh.at(i)) {
					channelsHigh.at(i) = image.at<cv::Vec3b>(y, x)[i];
				}
			}
		}
	}

	// Contrasting
	for (int i = 0; i < 3; ++i) {
		for (int y = 0; y < image.rows; ++y) {
			for (int x = 0; x < image.cols; ++x) {
				int newLow = image.at<cv::Vec3b>(y, x)[i] - channelsLow.at(i);
				int ratio = (channelMax - channelMin) / (channelsHigh[i] - channelsLow[i]);
				image.at<cv::Vec3b>(y, x)[i] = channelMin + newLow * ratio;
			}
		}
	}

	return image;
}

cv::Mat processImageLab04(const cv::Mat& image, const std::string& suffixFileName, const std::string& folder, PreColourReduction key)
{
	cv::imwrite(folder + "lab06_space_1" + key_names[key] + suffixFileName + ".png", image);

	cv::Mat processed;

	switch (key)
	{
	case GrayWorld:
		processed = grayWorld(image.clone());
		break;
	case AutoContrast:
		processed = autoContrast(image.clone());
		break;
	case GrayWorld_AutoContrast:
		processed = autoContrast(grayWorld(image.clone()));
		break;
	case AutoContrast_GrayWorld:
		processed = grayWorld(autoContrast(image.clone()));
		break;
	case None:
	default:
		processed = image.clone();
		break;
	}

	cv::imwrite(folder + "lab06_space" + key_names[key] + suffixFileName + ".png", processed);

	cv::cvtColor(processed, processed, cv::COLOR_BGR2GRAY);
	if (!cv::imwrite(folder + "lab06_space_2" + key_names[key] + suffixFileName + ".png", processed)) {
		std::cout << "ERROR " << folder << "lab06_space_2" << suffixFileName << ".png" << "\n";
	}

	cv::adaptiveThreshold(processed, processed, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 15, 0);
	cv::imwrite(folder + "lab06_space_3" + key_names[key] + suffixFileName + ".png", processed);

	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 3, 3 }));
	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 3, 3 }));
	cv::imwrite(folder + "lab06_space_4" + key_names[key] + suffixFileName + ".png", processed);

	cv::Mat labels;
	cv::Mat stats;
	cv::Mat centroids;
	cv::connectedComponentsWithStats(processed, labels, stats, centroids, 8, CV_32S);

	cv::Mat out = processed.clone();
	out = 0;

	int max = 0, maxInd = 0;
	int upperBorder = image.rows / 4;
	int lowerBorder = image.rows / 4 * 3;
	for (int y = 1; y < stats.rows; y += 1) {
		int area = stats.at<int>(y, cv::CC_STAT_AREA);
		int Y = stats.at<int>(y, cv::CC_STAT_TOP);

		if (Y < upperBorder || Y > lowerBorder) {
			continue;
		}

		if (area > max) {
			max = area;
			maxInd = y;
		}
	}

	int X = stats.at<int>(maxInd, cv::CC_STAT_LEFT);
	int Y = stats.at<int>(maxInd, cv::CC_STAT_TOP);
	int height = stats.at<int>(maxInd, cv::CC_STAT_HEIGHT);
	int width = stats.at<int>(maxInd, cv::CC_STAT_WIDTH);

	if (maxInd != 0) {
		cv::Mat cursor(out, cv::Rect(X, Y, width, height));
		cursor = 255;
	}

	processed = processed.mul(out);

	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 15, 15 }));
	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 10, 10 }));
	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 10, 10 }));
	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 15, 15 }));
	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_ELLIPSE, { 3, 3 }));
	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_ELLIPSE, { 3, 3 }));
	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_ELLIPSE, { 3, 3 }));
	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_ELLIPSE, { 3, 3 }));

	cv::imwrite(folder + "lab06_space_5" + key_names[key] + suffixFileName + ".png", processed);

	return processed;
}

static std::vector<float> accuracies;
static std::vector<float> precisions;

void drawAccuracyLab04(const cv::Mat& processed, const cv::Mat& mask, const std::string& suffixFileName, const std::string& folder, PreColourReduction key)
{
	cv::Mat diff(processed.rows, processed.cols, CV_8UC3, { 0, 0, 0 });
	int rightTrue = 0, rightFalse = 0;
	int wrongTrue = 0, wrongFalse = 0;

	for (int y = 0; y < processed.rows; y += 1) {
		for (int x = 0; x < processed.cols; x += 1) {
			// Right true
			if ((mask.at<uchar>(y, x) > 0) && (processed.at<uchar>(y, x) > 0)) {
				diff.at<cv::Vec3b>(y, x) = cv::Vec3b(255, 255, 255);
				++rightTrue;
			}
			// Right false
			if ((mask.at<uchar>(y, x) == 0) && (processed.at<uchar>(y, x) == 0)) {
				diff.at<cv::Vec3b>(y, x) = cv::Vec3b(0, 0, 0);
				++rightFalse;
			}
			// Wrong true
			if ((mask.at<uchar>(y, x) == 0) && (processed.at<uchar>(y, x) > 0)) {
				diff.at<cv::Vec3b>(y, x) = cv::Vec3b(0, 0, 255);
				++wrongTrue;
			}
			// Wrong false
			if ((mask.at<uchar>(y, x) > 0) && (processed.at<uchar>(y, x) == 0)) {
				diff.at<cv::Vec3b>(y, x) = cv::Vec3b(255, 0, 0);
				++wrongFalse;
			}
		}
	}

	double accuracy = (static_cast<double>(rightTrue) + rightFalse) / (static_cast<double>(rightTrue) + rightFalse + wrongFalse + wrongTrue);
	double precision = static_cast<double>(rightTrue) / (static_cast<double>(rightTrue) + wrongTrue);

	accuracies.push_back(accuracy);
	precisions.push_back(precision);

	std::string name = folder + "lab06_space_6" + key_names[key] + suffixFileName + ".png";
	std::cout << std::setprecision(3) << name << " " << "shows accuracy :\t" << accuracy << "\tand precision : \t" << precision << "\n";
	cv::imwrite(name, diff);

	cv::Mat colourOutput;
	double alpha = 0.5;
	double beta = 1 - alpha;

	std::string name_src = folder + "lab06_space_1" + key_names[key] + suffixFileName + ".png";
	cv::Mat src = cv::imread(name_src);
	
	cv::addWeighted(src, alpha, diff, beta, 0.0, colourOutput);
	cv::imwrite(folder + "lab06_space_7" + key_names[key] + suffixFileName + ".png", colourOutput);
}

void processImageDrawAccuracyLab04(const cv::Mat& image, const std::string& suffixFileName, const std::string& folder, PreColourReduction key)
{
	cv::Mat out = processImageLab04(image, suffixFileName, folder, key);
	cv::Mat mask = cv::imread("..\\data\\" + folder + "mask" + suffixFileName + ".png", cv::IMREAD_GRAYSCALE);

	if (mask.empty()) {
		return;
	}
	drawAccuracyLab04(out, mask, suffixFileName, folder, key);
}

float euclidean_dist(cv::Point2f first, cv::Point2f second)
{
	float dx = first.x - second.x;
	float dy = first.y - second.y;
	return std::sqrt(dx * dx + dy * dy);
}

cv::Point2f closestPoint(cv::Point2f anchor, const std::vector<cv::Point2f>& points)
{
	float dist = std::numeric_limits<float>::max();
	cv::Point2f closest;

	for (size_t i = 0; i < points.size(); ++i) {
		float new_dist = euclidean_dist(anchor, points.at(i));
		if (new_dist < dist) {
			dist = new_dist;
			closest = points.at(i);
		}
	}

	return closest;
}

cv::Point2f farestPoint(cv::Point2f anchor, const std::vector<cv::Point2f>& points)
{
	float dist = 0;
	cv::Point2f farest;

	for (size_t i = 0; i < points.size(); ++i) {
		float new_dist = euclidean_dist(anchor, points.at(i));
		if (new_dist > dist) {
			dist = new_dist;
			farest = points.at(i);
		}
	}

	return farest;
}

std::vector<cv::Point2f> sortDiagonalSquarePoints(std::vector<cv::Point2f> points) {

	if (points.size() != 4) {
		return points;
	}

	if ((points.at(0).y < points.at(2).y && points.at(1).y > points.at(3).y) ||
		(points.at(0).y > points.at(2).y && points.at(1).y < points.at(3).y)) {
		cv::Point2f tmp = points.at(3);
		points.at(3) = points.at(1);
		points.at(1) = tmp;
	}
	if ((points.at(0).x < points.at(1).x && points.at(2).x < points.at(3).x) ||
		(points.at(0).x > points.at(1).x && points.at(2).x > points.at(3).x)) {
		cv::Point2f tmp = points.at(2);
		points.at(2) = points.at(3);
		points.at(3) = tmp;
	}

	return points;
}

void diagonalMethod(std::vector<cv::Point2f>& diagonalPoints, const std::vector<cv::Point2f>& points, int stop = 200)
{
	if (points.empty()) {
		return;
	}

	std::vector<cv::Point2f> operationPoints = points;
	cv::Point2f anchor = operationPoints.at(0);

	int ind = 0;
	int repetition = 0;
	while (diagonalPoints.size() < 4 &&
		operationPoints.size() > 2 &&
		ind < stop) {
		cv::Point2f old_anchor = anchor;
		anchor = farestPoint(anchor, operationPoints);
		cv::Point2f later_anchor = farestPoint(anchor, operationPoints);

		if (old_anchor == later_anchor) {
			++repetition;
			if (repetition > 4) {
				repetition = 0;
				diagonalPoints.push_back(old_anchor);
				diagonalPoints.push_back(anchor);

				operationPoints.clear();
				bool contains = false;
				for (size_t i = 0; i < points.size(); ++i) {
					for (size_t j = 0; j < diagonalPoints.size(); ++j) {
						if (diagonalPoints.at(j) == points.at(i)) {
							contains = true;
							break;
						}
					}
					if (contains) {
						contains = false;
						continue;
					}

					operationPoints.push_back(points.at(i));
				}

				anchor = operationPoints.at(0);
				ind = 0;
			}
		}

		++ind;
	}

	diagonalPoints = sortDiagonalSquarePoints(diagonalPoints);
}

typedef std::map<char, std::vector<int>> mapXY;

mapXY getPointsFromJson(std::string& filePath, const std::string& topTag) {
	std::ifstream inputJSON(filePath);
	nlohmann::json json;
	inputJSON >> json;

	std::map<char, std::vector<int>> output;
	output['x'] = json[topTag]["regions"][0]["shape_attributes"]["all_points_x"].get<std::vector<int>>();
	output['y'] = json[topTag]["regions"][0]["shape_attributes"]["all_points_y"].get<std::vector<int>>();

	inputJSON.close();
	return output;
}

float getPerimeter(const std::vector<cv::Point2f>& angles) {
	float perimeter = 0;

	for (int i = 0; i < angles.size() - 1; i += 1) {
		float dist = euclidean_dist(angles.at(i), angles.at(i + 1));
		perimeter += dist;
	}

	float dist = euclidean_dist(angles.at(0), angles.at(angles.size() - 1));
	perimeter += dist;

	return perimeter;
}

float evaluateResultsLab05(const std::vector<cv::Point2f>& results, const std::vector<cv::Point2f>& ethalon)
{
	float maxDist = 0;

	for (int i = 0; i < results.size(); i += 1) {
		cv::Point2f closest = closestPoint(results.at(i), ethalon);

		float dist = euclidean_dist(closest, results.at(i));
		if (maxDist < dist) {
			maxDist = dist;
		}
	}

	float perimeter = getPerimeter(ethalon);

	return maxDist / perimeter;
}

std::vector<cv::Point2f> makePoint2fVector(const mapXY& XY)
{
	std::vector<cv::Point2f> output;

	for (int i = 0; i < XY.at('x').size(); i += 1) {
		cv::Point2f point(XY.at('x').at(i), XY.at('y').at(i));
		output.push_back(point);
	}

	return output;
}

void affectFileLab05(cv::Mat& image, const std::string& suffixFileName, const std::string& folder, const mapXY& XY, std::vector<float>& qualities, PreColourReduction key)
{
	cv::imwrite(folder + "lab06_edges_1" + suffixFileName + ".png", image);

	switch (key)
	{
	case GrayWorld:
		image = grayWorld(image.clone());
		break;
	case AutoContrast:
		image = autoContrast(image.clone());
		break;
	case GrayWorld_AutoContrast:
		image = autoContrast(grayWorld(image.clone()));
		break;
	case AutoContrast_GrayWorld:
		image = grayWorld(autoContrast(image.clone()));
		break;
	case None:
	default:
		break;
	}

	cv::imwrite(folder + "lab06_edges" + key_names[key] + suffixFileName + ".png", image);

	cv::Mat alphaOriginal;
	cv::cvtColor(image, alphaOriginal, cv::COLOR_BGR2BGRA);

	// Reduce
	cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
	cv::imwrite(folder + "lab06_edges_2" + key_names[key] + suffixFileName + ".png", image);

	cv::Mat edges;
	cv::GaussianBlur(image, image, { 3, 3 }, 0);
	cv::imwrite(folder + "lab06_edges_3" + key_names[key] + suffixFileName + ".png", image);

	int lowThreshold = 33;
	cv::Canny(image, edges, lowThreshold, lowThreshold * 3 + lowThreshold * 3 / 2, 3);
	cv::imwrite(folder + "lab06_edges_4" + key_names[key] + suffixFileName + ".png", edges);

	// Clear
	cv::Mat labels;
	cv::Mat stats;
	cv::Mat centroids;
	cv::connectedComponentsWithStats(edges, labels, stats, centroids, 8, CV_32S);

	int ind = 0;
	int constraint = 200;
	int upperBorder = edges.rows / 4;
	int lowerBorder = edges.rows / 4 * 3;

	int top = image.rows;
	int left = image.cols;
	for (int y = 1; y < stats.rows; y += 1) {
		int Y = stats.at<int>(y, cv::CC_STAT_TOP);
		int area = stats.at<int>(y, cv::CC_STAT_AREA);
		if ((Y < upperBorder || Y > lowerBorder) || (area <= constraint)) {
			continue;
		}

		if (top > stats.at<int>(y, cv::CC_STAT_TOP) &&
			left > stats.at<int>(y, cv::CC_STAT_LEFT)) {
			top = stats.at<int>(y, cv::CC_STAT_TOP);
			left = stats.at<int>(y, cv::CC_STAT_LEFT);
			ind = y;
		}
	}

	if (ind == 0) {
		return;
	}

	for (int y = 0; y < edges.rows; y += 1) {
		for (int x = 0; x < edges.cols; x += 1) {
			if (labels.at<int>(y, x) != ind) {
				edges.at<uchar>(y, x) = 0;
			}
		}
	}
	cv::imwrite(folder + "lab06_edges_5" + key_names[key] + suffixFileName + ".png", edges);

	cv::Mat alphaEdges;
	cv::cvtColor(edges, alphaEdges, cv::COLOR_GRAY2BGRA);

	for (int y = 0; y < alphaEdges.rows; y += 1) {
		for (int x = 0; x < alphaEdges.cols; x += 1) {
			if (alphaEdges.at<cv::Vec4b>(y, x) != cv::Vec4b(0, 0, 0, 255)) {
				alphaEdges.at<cv::Vec4b>(y, x) = cv::Vec4b(0, 235, 255, 255);
			}
		}
	}

	std::vector<cv::Point2f> corners;
	cv::goodFeaturesToTrack(edges, corners, 10, 0.01, 10, cv::Mat(), 3, 3, true, 0.04);

	// Creating a colour display mat
	cv::Mat colourDisplay(edges.rows, edges.cols, CV_8UC3);
	cv::cvtColor(edges, colourDisplay, cv::COLOR_GRAY2BGR);

	for (std::size_t i = 0; i < corners.size(); ++i) {
		circle(colourDisplay, corners[i], 4, cv::Scalar(50, 150, 0), cv::FILLED);
	}
	cv::imwrite(folder + "lab06_edges_6" + key_names[key] + suffixFileName + ".png", colourDisplay);

	std::vector<cv::Point2f> closestCorners;
	diagonalMethod(closestCorners, corners);
	for (std::size_t i = 0; i < closestCorners.size() / 2; ++i) {
		circle(colourDisplay, closestCorners[i], 4, cv::Scalar(255, 150, 0), cv::FILLED);
	}
	cv::imwrite(folder + "diag_edges_1" + key_names[key] + suffixFileName + ".png", colourDisplay);
	for (std::size_t i = closestCorners.size() / 2; i < closestCorners.size(); ++i) {
		circle(colourDisplay, closestCorners[i], 4, cv::Scalar(0, 150, 255), cv::FILLED);
	}
	cv::imwrite(folder + "diag_edges_2" + key_names[key] + suffixFileName + ".png", colourDisplay);

	cv::cvtColor(edges, colourDisplay, cv::COLOR_GRAY2BGR);
	for (std::size_t i = 0; i < closestCorners.size(); ++i) {
		circle(colourDisplay, closestCorners[i], 4, cv::Scalar(50, 150, 0), cv::FILLED);
	}
	cv::imwrite(folder + "lab06_edges_7" + key_names[key] + suffixFileName + ".png", colourDisplay);

	if (XY.empty()) {
		return;
	}

	std::vector<cv::Point2f> ethalonAngles = makePoint2fVector(XY);
	float quality = evaluateResultsLab05(closestCorners, ethalonAngles);

	std::cout << folder << " quality = " << quality << "\n";

	qualities.push_back(quality);

	cv::Mat colourOutput;
	double alpha = 0.5;
	double beta = 1 - alpha;

	cv::Mat alphaEthalon(image.size(), CV_8UC4, { 0, 0, 0, 255 });
	for (std::size_t i = 0; i < ethalonAngles.size() - 1; ++i) {
		cv::line(alphaEthalon, ethalonAngles.at(i), ethalonAngles.at(i + 1), { 0, 0, 225, 255 });
	}
	cv::line(alphaEthalon, ethalonAngles.at(0), ethalonAngles.at(ethalonAngles.size() - 1), { 0, 0, 225, 255 });

	cv::Mat computedOutput(image.size(), CV_8UC4);

	if (!closestCorners.empty()) {
		for (std::size_t i = 0; i < closestCorners.size() - 1; i += 1) {
			cv::line(computedOutput, closestCorners.at(i), closestCorners.at(i + 1), { 0, 235, 225, 255 });
		}
		cv::line(computedOutput, closestCorners.at(0), closestCorners.at(closestCorners.size() - 1), { 0, 235, 225, 255 });
	}
	cv::Mat tmp = alphaEthalon + computedOutput;
	cv::addWeighted(alphaOriginal, alpha, tmp, beta, 0.0, colourOutput);
	cv::imwrite(folder + "lab06_edges_8" + key_names[key] + suffixFileName + ".png", colourOutput);
}

void processFilesLab04(const std::vector<std::string>& files, PreColourReduction key)
{
	int fileInd = 1;
	for (int j = 0; j < 5; j += 1) {
		std::string path = "..\\data\\" + files.at(j);
		cv::VideoCapture cap(path, cv::CAP_FFMPEG);

		float frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
		float i = 2 * frames / 5;
		for (int j = 1; j < 4; j += 1) {
			cap.set(cv::CAP_PROP_POS_FRAMES, i);
			cv::Mat image;
			cap.read(image);
			cv::imwrite(".\\lab06\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\" + "lab06_space_0" + key_names[key] + "_" + std::to_string(j) + ".png", image);
			cv::resize(image, image, cv::Size(), 180. / image.cols, 320. / image.rows, cv::INTER_LINEAR);
			processImageDrawAccuracyLab04(image, "_" + std::to_string(j), ".\\lab06\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\", key);
			i += frames / 5;
		}
		fileInd += 1;
	}
	for (int j = 5; j < 15; j += 1) {
		std::string path = "..\\data\\" + files.at(j);
		cv::VideoCapture cap(path, cv::CAP_FFMPEG);

		float frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
		float i = 2 * frames / 5;
		for (int j = 1; j < 3; j += 1) {
			cap.set(cv::CAP_PROP_POS_FRAMES, i);
			cv::Mat image;
			cap.read(image);
			cv::imwrite(".\\lab06\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\" + "lab06_space_0" + key_names[key] + "_" + std::to_string(j) + ".png", image);
			cv::resize(image, image, cv::Size(), 180. / image.cols, 320. / image.rows, cv::INTER_LINEAR);
			processImageDrawAccuracyLab04(image, "_" + std::to_string(j), ".\\lab06\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\", key);
			i += 2 * frames / 5;
		}
		fileInd += 1;
	}
}

void calculateMD(const std::vector<float>& qualities)
{
	float sumQuality = 0.0f;
	std::map<float, float> p;
	std::vector<float> rounded_qualities;
	for (int i = 0; i < qualities.size(); i += 1) {
		float rounded = static_cast<int>(qualities.at(i) * 100) / 100.0f;
		p[rounded] += 1;
		if (p.at(rounded) == 1) {
			rounded_qualities.push_back(rounded);
		}
		sumQuality += qualities.at(i);
	}

	int sumMeet = 0;
	for (const float& value : rounded_qualities) {
		sumMeet += p.at(value);
	}

	for (const float& value : rounded_qualities) {
		p.at(value) = p.at(value) / sumMeet;
	}

	float M = 0.0f;
	for (int i = 0; i < rounded_qualities.size(); ++i) {
		M += rounded_qualities.at(i) * p.at(rounded_qualities.at(i));
	}

	float M2 = 0.0f;
	for (int i = 0; i < rounded_qualities.size(); ++i) {
		M2 += rounded_qualities.at(i) * rounded_qualities.at(i) * p.at(rounded_qualities.at(i));
	}
	float D = M2 - M * M;

	std::cout << "mean quality = " << sumQuality / qualities.size() << "\n";
	std::cout << "M = " << M << "\n";
	std::cout << "D^(1/2) = " << std::sqrt(D) << "\n";
}

void processFilesLab05(const std::vector<std::string>& files, const std::vector<std::string>& filesJSON, PreColourReduction key)
{
	int fileInd = 1;
	std::vector<float> qualities;
	for (int j = 0; j < 5; j += 1) {
		std::string path = "..\\data\\" + files.at(j);
		cv::VideoCapture cap(path, cv::CAP_FFMPEG);

		float frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
		float i = 2 * frames / 5;

		path = "..\\data\\" + filesJSON.at(j);

		std::vector<mapXY> videoXY;
		videoXY.push_back(getPointsFromJson(path, "1"));
		videoXY.push_back(getPointsFromJson(path, "2"));
		videoXY.push_back(getPointsFromJson(path, "3"));

		for (int j = 1; j < 4; j += 1) {
			cap.set(cv::CAP_PROP_POS_FRAMES, i);
			cv::Mat image;
			cap.read(image);
			cv::imwrite(".\\lab06\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\" + "lab06_edges_0" + key_names[key] + "_" + std::to_string(j) + ".png", image);
			cv::resize(image, image, cv::Size(), 180.0 / image.cols, 320.0 / image.rows, cv::INTER_LINEAR);
			affectFileLab05(image, "_" + std::to_string(j), ".\\lab06\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\", videoXY[j - 1], qualities, key);
			i += frames / 5;
		}
		fileInd += 1;
	}
	for (int j = 5; j < 15; j += 1) {
		std::string path = "..\\data\\" + files.at(j);
		cv::VideoCapture cap(path, cv::CAP_FFMPEG);

		if (j == 7) {
			int a = 1;
		}

		float frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
		float i = 2 * frames / 5;
		
		std::vector<mapXY> videoXY;
		if (j < filesJSON.size()) {
			path = "..\\data\\" + filesJSON.at(j);

			videoXY.push_back(getPointsFromJson(path, "1"));
			videoXY.push_back(getPointsFromJson(path, "2"));
		}

		for (int j = 1; j < 3; j += 1) {
			cap.set(cv::CAP_PROP_POS_FRAMES, i);
			cv::Mat image;
			cap.read(image);
			cv::imwrite(".\\lab06\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\" + "lab06_edges_0" + key_names[key] + "_" + std::to_string(j) + ".png", image);
			cv::resize(image, image, cv::Size(), 180.0 / image.cols, 320.0 / image.rows, cv::INTER_LINEAR);
			if (videoXY.empty()) {
				affectFileLab05(image, "_" + std::to_string(j), ".\\lab06\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\", mapXY(), qualities, key);
			}
			else {
				affectFileLab05(image, "_" + std::to_string(j), ".\\lab06\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\", videoXY[j - 1], qualities, key);
			}
			i += 2 * frames / 5;
		}
		fileInd += 1;
	}

	calculateMD(qualities);
}

int main()
{
	std::vector<std::string> files = { "IMG_3903.mp4",
																		"video_2022-03-22_11-41-23.mp4",
																		"video_2022-03-22_11-41-36.mp4",
																		"video_2022-03-22_11-41-41.mp4",
																		"video_2022-03-22_11-41-45.mp4",
																		"IMG_4515.mp4",
																		"IMG_4516.mp4",
																		"IMG_4517.mp4",
																		"IMG_4518.mp4",
																		"IMG_4526.mp4",
																		"IMG_4527.mp4",
																		"IMG_4528.mp4",
																		"IMG_4529.mp4",
																		"IMG_4531.mp4",
																		"IMG_4532.mp4"};

	std::vector<std::string> filesJSON = { "lab05_1.json",
																				 "lab05_2.json",
																				 "lab05_3.json",
																				 "lab05_4.json",
																				 "lab05_5.json",
																				 "lab06_6.json",
																				 "lab06_7.json",
																				 "lab06_8.json",
																				 "lab06_9.json",
																				 "lab06_10.json",
																				 "lab06_11.json",
																				 "lab06_12.json",
																				 "lab06_13.json",
																				 "lab06_14.json",
																				 "lab06_15.json"};

	int threeFrames = 3 * 5;
	int twoFrames = 2 * 10;

	processFilesLab04(files, PreColourReduction::None);
	std::cout << std::setprecision(3);
	std::cout << "Accuracy:\n";
	calculateMD(accuracies);
	std::cout << "Precision:\n";
	calculateMD(precisions);
	accuracies.clear();
	precisions.clear();

	processFilesLab04(files, PreColourReduction::GrayWorld);
	std::cout << "Accuracy:\n";
	calculateMD(accuracies);
	std::cout << "Precision:\n";
	calculateMD(precisions);
	accuracies.clear();
	precisions.clear();

	processFilesLab04(files, PreColourReduction::AutoContrast);
	std::cout << "Accuracy:\n";
	calculateMD(accuracies);
	std::cout << "Precision:\n";
	calculateMD(precisions);
	accuracies.clear();
	precisions.clear();

	processFilesLab04(files, PreColourReduction::GrayWorld_AutoContrast);
	std::cout << "Accuracy:\n";
	calculateMD(accuracies);
	std::cout << "Precision:\n";
	calculateMD(precisions);
	accuracies.clear();
	precisions.clear();

	processFilesLab04(files, PreColourReduction::AutoContrast_GrayWorld);
	std::cout << "Accuracy:\n";
	calculateMD(accuracies);
	std::cout << "Precision:\n";
	calculateMD(precisions);
	accuracies.clear();
	precisions.clear();

	processFilesLab05(files, filesJSON, PreColourReduction::None);

	processFilesLab05(files, filesJSON, PreColourReduction::GrayWorld);

	processFilesLab05(files, filesJSON, PreColourReduction::AutoContrast);

	processFilesLab05(files, filesJSON, PreColourReduction::GrayWorld_AutoContrast);

	processFilesLab05(files, filesJSON, PreColourReduction::AutoContrast_GrayWorld);

	return 0;
}