#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>

int methodOTSU(const cv::Mat& image) 
{
	int min = image.at<uchar>(0, 0);
	int max = image.at<uchar>(0, 0);

	for (int y = 0; y < image.rows; y += 1) {
		for (int x = 0; x < image.cols; x += 1) {
			int value = image.at<uchar>(y, x);

			if (value < min) {
				min = value;
			}

			if (value > max) {
				max = value;
			}
		}
	}

	int histSize = max - min + 1;
	std::vector<int> hist(histSize);

	for (int y = 0; y < image.rows; y += 1) {
		for (int x = 0; x < image.cols; x += 1) {
			hist.at(image.at<uchar>(y, x) - min) += 1;
		}
	}

	int m = 0, n = 0;
	for (int t = 0; t <= max - min; t += 1) {
		m += t * hist.at(t);
		n += hist.at(t);
	}

	float maxSigma = -1;
	int threshold = 0;

	int alpha1 = 0;
	int beta1 = 0;

	for (int t = 0; t < max - min; t += 1) {
		alpha1 += t * hist.at(t);
		beta1 += hist.at(t);

		float w1 = (float)beta1 / n;
		float a = (float)alpha1 / beta1 - (float)(m - alpha1) / (n - beta1);

		float sigma = w1 * (1 - w1) * a * a;

		if (sigma > maxSigma) {
			maxSigma = sigma;
			threshold = t;
		}
	}

	threshold += min;
	return threshold;
}

int meanBin(const cv::Mat& frame, const std::vector<int>& hist) 
{
	int min = frame.cols * frame.rows;
	int max = 0;
	int maxInd = 0, minInd = 0;
	for (int i = 0; i < hist.size(); i += 1) {
		if (hist[i] > max) {
			maxInd = i;
			max = hist[i];
		}
		if (hist[i] < min) {
			min = hist[i];
			minInd = i;
		}
	}

	int average = (max + min) / 2;
	int lowInd = maxInd;
	for (int i = maxInd; i >= 0; i -= 1) {
		if (hist[i] < average) {
			lowInd = i;
			break;
		}
	}

	int maxSecond = hist[lowInd];

	return (max + maxSecond) / 2;
}

void localBinBernsen(const cv::Mat& image, cv::Mat& out, const int width, const int height) 
{
	cv::Mat extended(image.rows + 2 * height, image.cols + 2 * width, CV_8UC1);
	image.copyTo(extended(cv::Rect(width, height, image.cols, image.rows)));

	int downHeight = extended.rows - height;
	int rightWidth = extended.cols - width;
	for (int y = height; y < downHeight; y += 1) {
		for (int x = rightWidth; x < extended.cols; x += 1) {
			extended.at<uchar>(y, x) = extended.at<uchar>(y, x - 1);
		}
	}

	for (int y = downHeight; y < extended.rows; y += 1) {
		for (int x = width; x < rightWidth; x += 1) {
			extended.at<uchar>(y, x) = extended.at<uchar>(y - 1, x);
		}
	}

	for (int y = height - 1; y >= 0; y -= 1) {
		for (int x = width; x < rightWidth; x += 1) {
			extended.at<uchar>(y, x) = extended.at<uchar>(y + 1, x);
		}
	}

	for (int y = height; y < downHeight; y += 1) {
		for (int x = width - 1; x >= 0; x -= 1) {
			extended.at<uchar>(y, x) = extended.at<uchar>(y, x + 1);
		}
	}

	cv::Rect rec(width / 2, height / 2, width, height);

	while (rec.x < rightWidth) {
		cv::Mat local = extended(rec);

		int maxIntensity = local.at<uchar>(0, 0); 
		int minIntensity = local.at<uchar>(0, 0);
		for (int y = 0; y < local.rows; y += 1) {
			for (int x = 0; x < local.cols; x += 1) {
				int intensity = local.at<uchar>(y, x);
				if (intensity > maxIntensity) {
					maxIntensity = intensity;
				}
				if (intensity < minIntensity) {
					minIntensity = intensity;
				}
			}
		}

		int meanIntensity = (static_cast<float>(maxIntensity) + minIntensity) / 2;
		
		for (int y = 0; y < local.rows; y += 1) {
			for (int x = 0; x < local.cols; x += 1) {
				if (local.at<uchar>(y, x) < meanIntensity) {
					local.at<uchar>(y, x) = 0;
				} else {
					local.at<uchar>(y, x) = 255;
				}
			}
		}

		rec.x += width;
		if (rec.x >= rightWidth && (rec.y + height) < downHeight) {
			rec.x = 0;
			rec.y += height;
		}
	}

	out = extended(cv::Rect(width, height, image.cols, image.rows));
}

cv::Mat processImage(const cv::Mat& image, const std::string& suffixFileName, const std::string& folder) 
{
	cv::imwrite(folder + "original" + suffixFileName + ".png", image);

	cv::Mat processed;
	cv::cvtColor(image, processed, cv::COLOR_BGR2GRAY);
	if (!cv::imwrite(folder + "gray" + suffixFileName + ".png", processed)) {
		std::cout << "ERROR " << folder << "gray" << suffixFileName << ".png" << "\n";
	}
	
	cv::adaptiveThreshold(processed, processed, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 15, 0);
	cv::imwrite(folder + "binary" + suffixFileName + ".png", processed);

	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 3, 3 }));
	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 3, 3 }));
	cv::imwrite(folder + "morph" + suffixFileName + ".png", processed);

	cv::Mat labels;
	cv::Mat stats;
	cv::Mat centroids;
	cv::connectedComponentsWithStats(processed, labels, stats, centroids, 8, CV_32S);

	cv::Mat out = processed.clone();
	out = 0;

	int max = 0, maxInd = 0;
	int upperBorder = image.rows / 4;
	int lowerBorder = image.rows / 4 * 3;
	for (int y = 1; y < stats.rows; y += 1) {
		int area = stats.at<int>(y, cv::CC_STAT_AREA);
		int Y = stats.at<int>(y, cv::CC_STAT_TOP);
		
		if (Y < upperBorder || Y > lowerBorder) {
			continue;
		}
		
		if (area > max) {
			max = area;
			maxInd = y;
		}
	}

	int X = stats.at<int>(maxInd, cv::CC_STAT_LEFT);
	int Y = stats.at<int>(maxInd, cv::CC_STAT_TOP);
	int height = stats.at<int>(maxInd, cv::CC_STAT_HEIGHT);
	int width = stats.at<int>(maxInd, cv::CC_STAT_WIDTH);
	
	if (maxInd != 0) {
		cv::Mat cursor(out, cv::Rect(X, Y, width, height));
		cursor = 255;
	}

	processed = processed.mul(out);

	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 15, 15 }));
	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 10, 10 }));
	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 10, 10 }));
	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_RECT, { 15, 15 }));
	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_ELLIPSE, { 3, 3 }));
	cv::dilate(processed, processed, cv::getStructuringElement(cv::MORPH_ELLIPSE, { 3, 3 }));
	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_ELLIPSE, { 3, 3 }));
	cv::erode(processed, processed, cv::getStructuringElement(cv::MORPH_ELLIPSE, { 3, 3 }));
	
	cv::imwrite(folder + "output" + suffixFileName + ".png", processed);

	return processed;
}

static double accuracies = 0.0;
static double precisions = 0.0;

void drawAccuracy(const cv::Mat& processed, const cv::Mat& mask, const std::string& suffixFileName, const std::string& folder)
{
	cv::Mat diff(processed.rows, processed.cols, CV_8UC3, { 0, 0, 0 });
	int rightTrue = 0, rightFalse = 0;
	int wrongTrue = 0, wrongFalse = 0;

	for (int y = 0; y < processed.rows; y += 1) {
		for (int x = 0; x < processed.cols; x += 1) {
			// Right true
			if ((mask.at<uchar>(y, x) > 0) && (processed.at<uchar>(y, x) > 0)) {
				diff.at<cv::Vec3b>(y, x) = cv::Vec3b(255, 255, 255);
				++rightTrue;
			}
			// Right false
			if ((mask.at<uchar>(y, x) == 0) && (processed.at<uchar>(y, x) == 0)) {
				diff.at<cv::Vec3b>(y, x) = cv::Vec3b(0, 0, 0);
				++rightFalse;
			}
			// Wrong true
			if ((mask.at<uchar>(y, x) == 0) && (processed.at<uchar>(y, x) > 0)) {
				diff.at<cv::Vec3b>(y, x) = cv::Vec3b(0, 0, 255);
				++wrongTrue;
			}
			// Wrong false
			if ((mask.at<uchar>(y, x) > 0) && (processed.at<uchar>(y, x) == 0)) {
				diff.at<cv::Vec3b>(y, x) = cv::Vec3b(255, 0, 0);
				++wrongFalse;
			}
		}
	}

	double accuracy = (static_cast<double>(rightTrue) + rightFalse) / (static_cast<double>(rightTrue) + rightFalse + wrongFalse + wrongTrue);
	double precision = static_cast<double>(rightTrue) / (static_cast<double>(rightTrue) + wrongTrue);

	accuracies += accuracy;
	precisions += precision;

	std::string name = folder + "diff" + suffixFileName + ".png";
	std::cout << std::setprecision(5) << name << " " << "shows accuracy :\t" << accuracy << "\tand precision : \t" << precision << "\n";
	cv::imwrite(name, diff);
}

void processImageDrawAccuracy(const cv::Mat& image, const std::string& suffixFileName, const std::string& folder)
{
	cv::Mat out = processImage(image, suffixFileName, folder);
	cv::Mat mask = cv::imread(folder + "mask" + suffixFileName + ".png", cv::IMREAD_GRAYSCALE);
	
	if (mask.empty()) {
		return;
	}
	drawAccuracy(out, mask, suffixFileName, folder);
}

void processFiles(const std::vector<std::string>& files)
{
	int fileInd = 1;
	for (const std::string& file : files) {
		std::string path = "..\\data\\" + file;
		cv::VideoCapture cap(path, cv::CAP_FFMPEG);

		float frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
		float i = 2 * frames / 5;
		for (int j = 1; j < 4; j += 1) {
			cap.set(cv::CAP_PROP_POS_FRAMES, i);
			cv::Mat image;
			cap.read(image);
			cv::imwrite(".\\lab04\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\" + "src" + "_" + std::to_string(j) + ".png", image);
			cv::resize(image, image, cv::Size(), 180. / image.cols, 320. / image.rows, cv::INTER_LINEAR);
			processImageDrawAccuracy(image,  "_" + std::to_string(j), ".\\lab04\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\");
			i += frames / 5;
		}
		fileInd += 1;
	}
}

std::vector<float> analyzeAccuracy(const std::vector<std::string>& files) 
{
	std::vector<float> out;
	int fileInd = 1;
	for (const std::string& file : files) {
		for (int j = 1; j < 4; j += 1) {
			std::string path = "lab04\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\";
			cv::Mat mask = cv::imread(path + "mask_" + std::to_string(j) + ".png", cv::IMREAD_GRAYSCALE);
			cv::Mat image = cv::imread(path + "output_" + std::to_string(j) + ".png", cv::IMREAD_GRAYSCALE);

			mask /= 255;
			cv::Mat common = image.mul(mask);
			common /= 255;

			int maskArea = 0, commonArea = 0;
			for (int y = 0; y < image.rows; y += 1) {
				for (int x = 0; x < image.cols; x += 1) {
					maskArea += mask.at<uchar>(y, x);
					commonArea += common.at<uchar>(y, x);
				}
			}

			out.push_back(static_cast<float>(commonArea) / maskArea);
		}
	}

	return out;
}

int main()
{
	std::vector<std::string> files = {"IMG_3903.mp4", 
																		"video_2022-03-22_11-41-23.mp4", 
																		"video_2022-03-22_11-41-36.mp4", 
																		"video_2022-03-22_11-41-41.mp4",
																		"video_2022-03-22_11-41-45.mp4"};

	processFiles(files);

	std::cout << std::setprecision(5) << "Accuracy mean = " << accuracies / (files.size() * 3)  << ";\tPrecision mean = " << precisions / (files.size() * 3) << "\n";
	return 0;
}