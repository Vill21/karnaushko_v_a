
#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <cmath>
#include <vector>

float brightness(const float& value) 
{
	return (3.0 / 2) * value  * std::cos(value);
}

int main() 
{
	cv::String path = "..\\data\\cross_0256x0256.png";
	cv::Mat img = cv::imread(path);
	
	cv::Mat vizFunc(512, 512, CV_8UC1, cv::Scalar(255));
	cv::line(vizFunc, cv::Point(0, 0), cv::Point(0, vizFunc.rows - 1), cv::Scalar(0), 2);
	cv::line(vizFunc, cv::Point(0, 0), cv::Point(3, 6), cv::Scalar(0), 2);
	cv::line(vizFunc, cv::Point(vizFunc.cols - 1, vizFunc.rows - 1), cv::Point(vizFunc.cols - 7, vizFunc.rows - 4), cv::Scalar(0), 2);
	cv::line(vizFunc, cv::Point(0, vizFunc.rows - 1), cv::Point(vizFunc.cols - 1, vizFunc.rows - 1), cv::Scalar(0), 2);
	cv::putText(vizFunc, "Old brightness", { vizFunc.cols - 135, vizFunc.rows - 5 }, cv::FONT_HERSHEY_PLAIN, 1, { 0 });
	cv::putText(vizFunc, "New brightness", { 5, 10 }, cv::FONT_HERSHEY_PLAIN, 1, { 0 });

	for (int x = 0; x < vizFunc.cols; x += 1) {
		int y = 511 - brightness(x / 511.0) * 511;
		vizFunc.at<uchar>(y, x) = 0;
	}

	cv::Mat imgGray;
	cv::cvtColor(img, imgGray, cv::COLOR_BGR2GRAY);

	std::vector<int> lookUpTable(256);
	for (int i = 0; i < 256; i += 1) {
		lookUpTable.at(i) = (int)(brightness(i / 255.0) * 255);
	}

	cv::Mat imgGrayBright;
	cv::LUT(imgGray, lookUpTable, imgGrayBright);

	cv::Mat channels[3];
	cv::split(img, channels);

	for (auto& channel : channels) {
		cv::LUT(channel, lookUpTable, channel);
	}

	cv::Mat imgBright;
	cv::merge(channels, 3, imgBright);

	cv::imwrite("lab03_rgb.png", img);
	cv::imwrite("lab03_viz_func.png", vizFunc);
	cv::imwrite("lab03_gre.png", imgGray);
	cv::imwrite("lab03_gre_res.png", imgGrayBright);
	cv::imwrite("lab03_rgb_res.png", imgBright);
	return 0;
}