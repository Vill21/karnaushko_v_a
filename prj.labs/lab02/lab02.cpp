#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <vector>
#include <map>

cv::Mat mosaic(cv::Mat& img) 
{
	cv::Mat imgChannels[3];
	cv::split(img, imgChannels);

	const cv::Mat redZero = imgChannels[2] * 0;
	const cv::Mat blueZero = imgChannels[0] * 0;
	const cv::Mat greenZero = imgChannels[1] * 0;

	cv::Mat imgRed;
	cv::merge(std::vector<cv::Mat>{ blueZero, greenZero, imgChannels[2] }, imgRed);

	cv::Mat imgBlue;
	cv::merge(std::vector<cv::Mat>{ imgChannels[0], greenZero, redZero }, imgBlue);

	cv::Mat imgGreen;
	cv::merge(std::vector<cv::Mat>{ blueZero, imgChannels[1], redZero }, imgGreen);

	cv::Mat imgMosaic;
	cv::Mat horizontalUpper;
	cv::Mat horizontalLower;
	cv::hconcat(img, imgRed, horizontalUpper);
	cv::hconcat(imgGreen, imgBlue, horizontalLower);
	cv::vconcat(horizontalUpper, horizontalLower, imgMosaic);

	return imgMosaic;
}

std::vector<int> getHist(const cv::Mat& img, const int depth) 
{
	std::vector<int> hist(depth);

	//count the frequence of each intensity
	for (int y = 0; y < img.rows; y += 1) {
		for (int x = 0; x < img.cols; x += 1) {
			hist[img.at<uchar>(y, x)] += 1;
		}
	}

	return hist;
}

void drawHist(cv::Mat& img,	
	            const std::vector<int>& hist, 
	            const cv::Scalar& colour = { 255 }, 
	            const int widthArg = 1, 
	            const double heightArg = 1) 
{
	for (int x = 0; x < img.cols; x += widthArg) {
		cv::Point start(x, img.rows);
		int yEnd = (int)(img.rows - (double)hist[x] * heightArg);
		cv::Point end(x, yEnd);
		cv::line(img, start, end, colour, widthArg);
	}
}

cv::Mat mosaicHist(cv::Mat& img, const int channels, const int depth)
{
	std::vector<cv::Mat> imgChannels(channels);
	cv::split(img, imgChannels);

	std::map<int, std::vector<int>> hist;
	for (int channel = 0; channel < channels; channel += 1) {
		hist[channel] = getHist(imgChannels[channel], depth);
	}

	//find max element
	std::vector<int> maxCount(channels);
	for (int channel = 0; channel < channels; channel += 1) {
		for (int x = 0; x < depth; x += 1) {
			if (maxCount[channel] < hist[channel][x]) {
				maxCount[channel] = hist[channel][x];
			}
		}
	}

	std::vector<cv::Mat> imgHist(channels);
	for (int channel = 0; channel < channels; channel += 1) {
		imgHist[channel] = cv::Mat(img.cols, img.rows, CV_8UC3, cv::Scalar(0, 0, 0));
	}

	const int widthArg = 1;
	const double heightArg = (double)depth / 3 * 2;

	drawHist(imgHist[0], hist[0], { depth - 1.0, 0, 0 }, widthArg, heightArg / maxCount[0]);
	drawHist(imgHist[1], hist[1], { 0, depth - 1.0, 0 }, widthArg, heightArg / maxCount[1]);
	drawHist(imgHist[2], hist[2], { 0, 0, depth - 1.0 }, widthArg, heightArg / maxCount[2]);

	cv::Mat output;
	cv::hconcat(imgHist, output);

	return output;
}

int main() 
{
	std::string path = "C:\\HomeWorkFlow\\karnaushko_v_a\\data\\cross_0256x0256.png";
	cv::Mat img = cv::imread(path);

	std::vector<int> params;
	params.push_back(cv::IMWRITE_JPEG_QUALITY);
	params.push_back(25);
	cv::imwrite("cross_0256x0256_025.jpg", img, params);

	path = cv::samples::findFile("cross_0256x0256_025.jpg");
	cv::Mat imgJPEG = cv::imread(path);

	cv::Mat imgMosaic = mosaic(img);
	cv::Mat imgJPEGMosaic = mosaic(imgJPEG);

	cv::imwrite("cross_0256x0256_png_channels.png", imgMosaic);
	cv::imwrite("cross_0256x0256_jpg_channels.png", imgJPEGMosaic);

	// Hists of png and jpg images

	cv::Mat pngHist = mosaicHist(img, 3, 256);
	cv::Mat jpgHist = mosaicHist(imgJPEG, 3, 256);
	cv::Mat combinedHist;

	cv::vconcat(pngHist, jpgHist, combinedHist);
	cv::imwrite("cross_0256x0256_hists.png", combinedHist);
	return 0;
}