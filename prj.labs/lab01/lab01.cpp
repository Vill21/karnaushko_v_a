#include <opencv2/opencv.hpp>
#include <cmath>
#include <chrono>
#include <iostream>

int main() {
  cv::Mat img(180, 768, CV_8UC1);
  cv::Rect2d rc(0, 0, 768, 60);
  cv::Mat I_1(img(rc));
  I_1 = 0;

  for (int y = 0; y < I_1.rows; y += 1) {
    for (int x = 0; x < I_1.cols; x += 1) {
      I_1.at<uchar>(y, x) += x / 3;
    }
  }

  rc.y += rc.height;

  //gamma
  const float gamma = 2.2;

  //G_1 - gamma-correction and pow()
  cv::Mat G_1(img(rc));
  cv::Mat I_1f;
  cv::Mat tmp;
  I_1.convertTo(I_1f, CV_32FC1, 1.0 / 255);

  auto start = std::chrono::steady_clock::now();
  cv::pow(I_1f, gamma, tmp);
  tmp.convertTo(G_1, CV_8UC1, 255);
  auto end = std::chrono::steady_clock::now();
  std::chrono::duration<double> elapsedSeconds = end - start;
  std::cout << "G1 time: " << elapsedSeconds.count() << "s\n";

  rc.y += rc.height;

  //G_2 - gamma-correction each pixel
  cv::Mat G_2(img(rc));

  start = std::chrono::steady_clock::now();
  for (int y = 0; y < G_2.rows; y += 1) {
    for (int x = 0; x < G_2.cols; x += 1) {
      G_2.at<uchar>(y, x) = (uchar)(pow(I_1f.at<float>(y, x), gamma) * 255);
    }
  }
  end = std::chrono::steady_clock::now();
  elapsedSeconds = end - start;
  std::cout << "G2 time: " << elapsedSeconds.count() << "s\n";

  // save result
  cv::imwrite("lab01.png", img);
}
