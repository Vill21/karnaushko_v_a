#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <cmath>

cv::Mat drawField() 
{
  const int squareSide = 150;
  const int radius = squareSide / 3;

  cv::Scalar black{ 0 };
  cv::Scalar white{ 255 };
  cv::Scalar gray{ 127 };

  cv::Mat field(3 * squareSide, 2 * squareSide, CV_8UC1);

  cv::Rect rect(0, 0, squareSide, squareSide);
  
  cv::Mat cursor = field(rect);
  cursor = black;
  cv::circle(field, { rect.x + rect.width / 2, rect.y + rect.height / 2 }, radius, white, -1);

  rect.x += rect.width;
  cursor = field(rect);
  cursor = white;
  cv::circle(field, { rect.x + rect.width / 2, rect.y + rect.height / 2 }, radius, black, -1);

  rect.x = 0;
  rect.y += rect.height;
  cursor = field(rect);
  cursor = gray;
  cv::circle(field, { rect.x + rect.width / 2, rect.y + rect.height / 2 }, radius, black, -1);

  rect.x += rect.width;
  cursor = field(rect);
  cursor = black;
  cv::circle(field, { rect.x + rect.width / 2, rect.y + rect.height / 2 }, radius, gray, -1);

  rect.x = 0;
  rect.y += rect.height;
  cursor = field(rect);
  cursor = white;
  cv::circle(field, { rect.x + rect.width / 2, rect.y + rect.height / 2 }, radius, gray, -1);

  rect.x += rect.width;
  cursor = field(rect);
  cursor = gray;
  cv::circle(field, { rect.x + rect.width / 2, rect.y + rect.height / 2 }, radius, white, -1);

  return field;
}

int main() 
{
  cv::Mat picture = drawField();
  cv::imwrite("field.png", picture);

  cv::Mat I1 = (cv::Mat_<double>(3, 3) << 1, 0, -1, 2, 0, -2, 1, 0, -1);
  cv::Mat I2 = (cv::Mat_<double>(3, 3) << 1, 2, 1, 0, 0, 0, -1, -2, -1);

  cv::Mat filteredI1 = picture.clone();
  filteredI1.convertTo(filteredI1, CV_32FC1, 1. / 255);
  cv::Mat filteredI2 = picture.clone();
  filteredI2.convertTo(filteredI2, CV_32FC1, 1. / 255);

  // [0, 1] -> [-4, 4]
  cv::filter2D(filteredI1, filteredI1, -1, I1);
  cv::filter2D(filteredI2, filteredI2, -1, I2);

  // [-4, 4] -> [0, 4 * sqrt(2)]
  cv::Mat geomMean(picture.size(), CV_32FC1);
  for (int y = 0; y < picture.rows; ++y) {
    for (int x = 0; x < picture.cols; ++x) {
      geomMean.at<float>(y, x) = std::sqrt(std::pow(filteredI1.at<float>(y, x), 2) + std::pow(filteredI2.at<float>(y, x), 2));
    }
  }

  // normalize back to [0, 1] and then [0, 255]
  cv::normalize(geomMean, geomMean, 1.0, 0.0, cv::NORM_MINMAX);
  geomMean.convertTo(geomMean, CV_8UC1, 255);
  cv::imwrite("geometricMean.png", geomMean);

  cv::normalize(filteredI1, filteredI1, 1.0, 0.0, cv::NORM_MINMAX);
  filteredI1.convertTo(filteredI1, CV_8UC1, 255);
  cv::imwrite("filteredI1.png", filteredI1);

  cv::normalize(filteredI2, filteredI2, 1.0, 0.0, cv::NORM_MINMAX);
  filteredI2.convertTo(filteredI2, CV_8UC1, 255);
  cv::imwrite("filteredI2.png", filteredI2);

  return 0;
}
