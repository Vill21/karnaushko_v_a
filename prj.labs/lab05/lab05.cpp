#include <opencv2/opencv.hpp>
#include <vector>
#include <iostream>
#include <limits>
#include <fstream>
#include <nlohmann/json.hpp>
#include <map>

float euclidean_dist(cv::Point2f first, cv::Point2f second) 
{
	float dx = first.x - second.x;
	float dy = first.y - second.y;
	return std::sqrt(dx * dx + dy * dy);
}

cv::Point2f closestPoint(cv::Point2f anchor, const std::vector<cv::Point2f>& points)
{
	float dist = std::numeric_limits<float>::max();
	cv::Point2f closest;

	for (size_t i = 0; i < points.size(); ++i) {
		float new_dist = euclidean_dist(anchor, points.at(i));
		if (new_dist < dist) {
			dist = new_dist;
			closest = points.at(i);
		}
	}

	return closest;
}

cv::Point2f farestPoint(cv::Point2f anchor, const std::vector<cv::Point2f>& points)
{
	float dist = 0;
	cv::Point2f farest;

	for (size_t i = 0; i < points.size(); ++i) {
		float new_dist = euclidean_dist(anchor, points.at(i));
		if (new_dist > dist) {
			dist = new_dist;
			farest = points.at(i);
		}
	}

	return farest;
}

std::vector<cv::Point2f> sortDiagonalSquarePoints(std::vector<cv::Point2f> points) {

	if ((points.at(0).y < points.at(2).y && points.at(1).y > points.at(3).y) ||
		(points.at(0).y > points.at(2).y && points.at(1).y < points.at(3).y)) {
		cv::Point2f tmp = points.at(3);
		points.at(3) = points.at(1);
		points.at(1) = tmp;
	}
	if ((points.at(0).x < points.at(1).x && points.at(2).x < points.at(3).x) ||
		(points.at(0).x > points.at(1).x && points.at(2).x > points.at(3).x)) {
		cv::Point2f tmp = points.at(2);
		points.at(2) = points.at(3);
		points.at(3) = tmp;
	}

	return points;
}

void diagonalMethod(std::vector<cv::Point2f>& diagonalPoints, const std::vector<cv::Point2f>& points, int stop = 200)
{
	if (points.empty()) {
		return;
	}

	std::vector<cv::Point2f> operationPoints = points;
	cv::Point2f anchor = operationPoints.at(0);
	
	int ind = 0;
	int repetition = 0;
	while (diagonalPoints.size() < 4 && 
				 operationPoints.size() > 1 &&
				 ind < stop) {
		cv::Point2f old_anchor = anchor;
		anchor = farestPoint(anchor, operationPoints);
		cv::Point2f later_anchor = farestPoint(anchor, operationPoints);

		if (old_anchor == later_anchor) {
			++repetition;
			if (repetition > 4) {
				repetition = 0;
				diagonalPoints.push_back(old_anchor);
				diagonalPoints.push_back(anchor);

				operationPoints.clear();
				bool contains = false;
				for (size_t i = 0; i < points.size(); ++i) {
					for (size_t j = 0; j < diagonalPoints.size(); ++j) {
						if (diagonalPoints.at(j) == points.at(i)) {
							contains = true;
							break;
						}
					}
					if (contains) {
						contains = false;
						continue;
					}

					operationPoints.push_back(points.at(i));
				}

				anchor = operationPoints.at(0);
				ind = 0;
			}
		}

		++ind;
	}

	diagonalPoints = sortDiagonalSquarePoints(diagonalPoints);
}

typedef std::map<char, std::vector<int>> mapXY;

mapXY getPointsFromJson(std::string& filePath, const std::string& topTag) {
	std::ifstream inputJSON(filePath);
	nlohmann::json json;
	inputJSON >> json;

	std::map<char, std::vector<int>> output;
	output['x'] = json[topTag]["regions"][0]["shape_attributes"]["all_points_x"].get<std::vector<int>>();
	output['y'] = json[topTag]["regions"][0]["shape_attributes"]["all_points_y"].get<std::vector<int>>();

	inputJSON.close();
	return output;
}

float getPerimeter(const std::vector<cv::Point2f>& angles) {
	float perimeter = 0;
	
	for (int i = 0; i < angles.size() - 1; i += 1) {
		float dist = euclidean_dist(angles.at(i), angles.at(i + 1));
		perimeter += dist;
	}

	float dist = euclidean_dist(angles.at(0), angles.at(angles.size() - 1));
	perimeter += dist;

	return perimeter;
}

float evaluateResults(const std::vector<cv::Point2f>& results, const std::vector<cv::Point2f>& ethalon)
{
	float maxDist = 0;

	for (int i = 0; i < results.size(); i += 1) {
		cv::Point2f closest = closestPoint(results.at(i), ethalon);

		float dist = euclidean_dist(closest, results.at(i));
		if (maxDist < dist) {
			maxDist = dist;
		}
	}

	float perimeter = getPerimeter(ethalon);

	return maxDist / perimeter;
}

std::vector<cv::Point2f> makePoint2fVector(const mapXY& XY) 
{
	std::vector<cv::Point2f> output;

	for (int i = 0; i < XY.at('x').size(); i += 1) {
		cv::Point2f point(XY.at('x').at(i), XY.at('y').at(i));
		output.push_back(point);
	}

	return output;
}

void affectFile(cv::Mat& image, const std::string& suffixFileName, const std::string& folder, const mapXY& XY, std::vector<float>& qualities) 
{
	cv::imwrite(folder + "lab05_1" + suffixFileName + ".png", image);

	cv::Mat alphaOriginal;
	cv::cvtColor(image, alphaOriginal, cv::COLOR_BGR2BGRA);

	// Reduce
	cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
	cv::imwrite(folder + "lab05_2" + suffixFileName + ".png", image);

	cv::Mat edges;
	cv::GaussianBlur(image, image, { 3, 3 }, 0);
	cv::imwrite(folder + "lab05_3" + suffixFileName + ".png", image);

	int lowThreshold = 33;
	cv::Canny(image, edges, lowThreshold, lowThreshold * 3 + lowThreshold * 3 / 2, 3);
	cv::imwrite(folder + "lab05_4" + suffixFileName + ".png", edges);

	// Clear
	cv::Mat labels;
	cv::Mat stats;
	cv::Mat centroids;
	cv::connectedComponentsWithStats(edges, labels, stats, centroids, 8, CV_32S);

	int ind = 0;
	int constraint = 200;
	int upperBorder = edges.rows / 4;
	int lowerBorder = edges.rows / 4 * 3;

	int top = image.rows;
	int left = image.cols;
	for (int y = 1; y < stats.rows; y += 1) {
		int Y = stats.at<int>(y, cv::CC_STAT_TOP);
		int area = stats.at<int>(y, cv::CC_STAT_AREA);
		if ((Y < upperBorder || Y > lowerBorder) || (area <= constraint)) {
			continue;
		}

		if (top > stats.at<int>(y, cv::CC_STAT_TOP) && 
			left > stats.at<int>(y, cv::CC_STAT_LEFT)) {
			top = stats.at<int>(y, cv::CC_STAT_TOP);
			left = stats.at<int>(y, cv::CC_STAT_LEFT);
			ind = y;
		}
	}

	if (ind == 0) {
		return;
	}

	for (int y = 0; y < edges.rows; y += 1) {
		for (int x = 0; x < edges.cols; x += 1) {
			if (labels.at<int>(y, x) != ind) {
				edges.at<uchar>(y, x) = 0;
			}
		}
	}
	cv::imwrite(folder + "lab05_5" + suffixFileName + ".png", edges);

	cv::Mat alphaEdges;
	cv::cvtColor(edges, alphaEdges, cv::COLOR_GRAY2BGRA);

	for (int y = 0; y < alphaEdges.rows; y += 1) {
		for (int x = 0; x < alphaEdges.cols; x += 1) {
			if (alphaEdges.at<cv::Vec4b>(y, x) != cv::Vec4b(0, 0, 0, 255)) {
				alphaEdges.at<cv::Vec4b>(y, x) = cv::Vec4b(0, 235, 255, 255);
			}
		}
	}

	std::vector<cv::Point2f> corners;
	cv::goodFeaturesToTrack(edges, corners, 10, 0.01, 10, cv::Mat(), 3, 3, true, 0.04);

	// Creating a colour display mat
	cv::Mat colourDisplay(edges.rows, edges.cols, CV_8UC3);
	cv::cvtColor(edges, colourDisplay, cv::COLOR_GRAY2BGR);

	for (std::size_t i = 0; i < corners.size(); ++i) {
		circle(colourDisplay, corners[i], 4, cv::Scalar(50, 150, 0), cv::FILLED);
	}
	cv::imwrite(folder + "lab05_6" + suffixFileName + ".png", colourDisplay);

	std::vector<cv::Point2f> closestCorners;
	diagonalMethod(closestCorners, corners);
	for (std::size_t i = 0; i < closestCorners.size() / 2; ++i) {
		circle(colourDisplay, closestCorners[i], 4, cv::Scalar(255, 150, 0), cv::FILLED);
	}
	cv::imwrite(folder + "diag_1" + suffixFileName + ".png", colourDisplay);
	for (std::size_t i = closestCorners.size() / 2; i < closestCorners.size(); ++i) {
		circle(colourDisplay, closestCorners[i], 4, cv::Scalar(0, 150, 255), cv::FILLED);
	}
	cv::imwrite(folder + "diag_2" + suffixFileName + ".png", colourDisplay);

	cv::cvtColor(edges, colourDisplay, cv::COLOR_GRAY2BGR);
	for (std::size_t i = 0; i < closestCorners.size(); ++i)	{
		circle(colourDisplay, closestCorners[i], 4, cv::Scalar(50, 150, 0), cv::FILLED);
	}
	cv::imwrite(folder + "lab05_7" + suffixFileName + ".png", colourDisplay);

	if (XY.empty()) {
		return;
	}

	std::vector<cv::Point2f> ethalonAngles = makePoint2fVector(XY);
	float quality = evaluateResults(closestCorners, ethalonAngles);

	std::cout << folder << " quality = " << quality << "\n";

	qualities.push_back(quality);

	cv::Mat colourOutput;
	double alpha = 0.5;
	double beta = 1 - alpha;

	cv::Mat alphaEthalon(image.size(), CV_8UC4, {0, 0, 0, 255});
	for (std::size_t i = 0; i < ethalonAngles.size() - 1; ++i) {
		cv::line(alphaEthalon, ethalonAngles.at(i), ethalonAngles.at(i + 1), { 0, 0, 225, 255 });
	}
	cv::line(alphaEthalon, ethalonAngles.at(0), ethalonAngles.at(ethalonAngles.size() - 1), { 0, 0, 225, 255 });

	cv::Mat computedOutput(image.size(), CV_8UC4);

	for (std::size_t i = 0; i < closestCorners.size() - 1; i += 1) {
		cv::line(computedOutput, closestCorners.at(i), closestCorners.at(i + 1), { 0, 235, 225, 255 });
	}
	cv::line(computedOutput, closestCorners.at(0), closestCorners.at(closestCorners.size() - 1), { 0, 235, 225, 255 });

	cv::Mat tmp = alphaEthalon + computedOutput;
	cv::addWeighted(alphaOriginal, alpha, tmp, beta, 0.0, colourOutput);
	cv::imwrite(folder + "lab05_8" + suffixFileName + ".png", colourOutput);
}

void processFiles(const std::vector<std::string>& files, const std::vector<std::string>& filesJSON)
{
	int fileInd = 1;
	std::vector<float> qualities;
	for (int j = 0; j < files.size(); j += 1) {
		std::string path = "..\\data\\" + files.at(j);
		cv::VideoCapture cap(path, cv::CAP_FFMPEG);

		float frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
		float i = 2 * frames / 5;

		path = "..\\data\\" + filesJSON.at(j);
		
		std::vector<mapXY> videoXY;
		videoXY.push_back(getPointsFromJson(path, "1"));
		videoXY.push_back(getPointsFromJson(path, "2"));
		videoXY.push_back(getPointsFromJson(path, "3"));

		for (int j = 1; j < 4; j += 1) {
			cap.set(cv::CAP_PROP_POS_FRAMES, i);
			cv::Mat image;
			cap.read(image);
			cv::imwrite(".\\lab05\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\" + "lab05_0" + "_" + std::to_string(j) + ".png", image);
			cv::resize(image, image, cv::Size(), 180.0 / image.cols, 320.0 / image.rows, cv::INTER_LINEAR);
			affectFile(image, "_" + std::to_string(j), ".\\lab05\\file" + std::to_string(fileInd) + "\\" + std::to_string(j) + "\\", videoXY[j - 1], qualities);
			i += frames / 5;
		}
		fileInd += 1;
	} 
	
	float sumQuality = 0;
	for (int i = 0; i < qualities.size(); i += 1) {
		sumQuality += qualities.at(i);
	}

	std::cout << "mean quality = " << sumQuality / qualities.size() << "\n";
}

int lowThreshold = 0;
int maxThreshold = 100;
int ratio = 3;
cv::Mat src, dst;
cv::Mat srcGray, detectedEdges;
const std::string winName = "track";

static void track(int, void*)
{
	cv::GaussianBlur(srcGray, detectedEdges, { 3, 3 }, 0);
	
	cv::Canny(detectedEdges, detectedEdges, lowThreshold, lowThreshold * ratio + lowThreshold * 3 / 2, 3);

	dst = 0;

	src.copyTo(dst, detectedEdges);

	cv::imshow(winName, dst);
}

#define LOCK 0;

int main() 
{
	std::vector<std::string> files = {"IMG_3903.mp4",
																		"video_2022-03-22_11-41-23.mp4",
																		"video_2022-03-22_11-41-36.mp4",
																		"video_2022-03-22_11-41-41.mp4",
																		"video_2022-03-22_11-41-45.mp4"};

	std::vector<std::string> filesJSON = { "lab05_1.json",
																				 "lab05_2.json",
																				 "lab05_3.json",
																				 "lab05_4.json",
																				 "lab05_5.json" };
	processFiles(files, filesJSON);

#if LOCK

	src = cv::imread("lab05\\file5\\1\\lab05_1_1.png");

	dst.create(src.size(), src.type());

	cv::cvtColor(src, srcGray, cv::COLOR_BGR2GRAY);

	cv::namedWindow(winName, cv::WINDOW_AUTOSIZE);

	cv::createTrackbar("Min Threshold:", winName, &lowThreshold, maxThreshold, track);

	track(0, 0);

	cv::waitKey(0);

#endif

	return 0;
}

// 44 44